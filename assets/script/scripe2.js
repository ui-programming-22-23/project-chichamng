const canvas = document.getElementById("mycanvas2");
const context = canvas.getContext("2d");



const username = localStorage.getItem('username');

function addName() {
    let header = document.getElementById("header");
        header.innerHTML = username + ",  This is the last room, BREAK the code then ESCAPE!";
}

addName();

//for timer
var level = localStorage.getItem('usermode');
var countDownTime = 0;

if(level == "1"){
    countDownTime = 60; //1 minute count down 
}
else if(level == "2"){
    countDownTime = 30;
}
else if(level == "3"){
    countDownTime = 60;
}

let lastUpdateTime = Date.now();
let timerRunning = true;

//forPIN
var realPin1;
var realPin2;
var realPin3;
var realPin4;
var realPin5;
var realPin6;
var realPin7;

if(level == "1" || level == "2"){
    realPin1 = Math.floor(Math.random() * 10);
    realPin2 = Math.floor(Math.random() * 10);
    realPin3 = Math.floor(Math.random() * 10);
    realPin4 = Math.floor(Math.random() * 10);
    realPin5 = Math.floor(Math.random() * 10);
    realPin6 = Math.floor(Math.random() * 10);
    realPin7 = Math.floor(Math.random() * 10);
    }
    
    else if(level == "3"){
        realPin1 = getRandomInt(1, 20); 
        realPin2 = getRandomInt(1, 20); 
        realPin3 = getRandomInt(1, 20); 
        realPin4 = getRandomInt(1, 20); 
        realPin5 = getRandomInt(1, 20); 
        realPin6 = getRandomInt(1, 20); 
        realPin7 = getRandomInt(1, 20); 
    }
    
    else{
        realPin1 = getRandomInt(1, 30); 
        realPin2 = getRandomInt(1, 30); 
        realPin3 = getRandomInt(1, 30); 
        realPin4 = getRandomInt(1, 30); 
        realPin5 = getRandomInt(1, 30); 
        realPin6 = getRandomInt(1, 30); 
        realPin7 = getRandomInt(1, 30); 
    }

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }


//for random number
var randomNumber1 = Math.floor(Math.random() * 10);
var randomNumber2 = Math.floor(Math.random() * 10);
var randomNumber3 = Math.floor(Math.random() * 10);

if(level == "1" || level == "2"){
    randomNumber1 = Math.floor(Math.random() * 10);
    randomNumber2 = Math.floor(Math.random() * 10);
    randomNumber3 = Math.floor(Math.random() * 10);
    }
    
    else if(level == "3"){
        randomNumber1 = getRandomInt(1, 20);
        randomNumber2 = getRandomInt(1, 20);
        randomNumber3 = getRandomInt(1, 20);
    }
    
    
    else{
        randomNumber1 = getRandomInt(1, 30);
        randomNumber2 = getRandomInt(1, 30);
        randomNumber3 = getRandomInt(1, 30);
    }

//set specify area for number appear
var areaX = 50;
var areaY = 150;
var areaWidth = 800;
var areaHeight = 400;

var x1 = Math.floor(Math.random() * areaWidth) + areaX;
var y1 = Math.floor(Math.random() * areaHeight) + areaY;
var x2 = Math.floor(Math.random() * areaWidth) + areaX;
var y2 = Math.floor(Math.random() * areaHeight) + areaY;
var x3 = Math.floor(Math.random() * areaWidth) + areaX;
var y3 = Math.floor(Math.random() * areaHeight) + areaY;


//code
var playernumber = 11;
var fillColor1 = "#3E3E3E";
var fillColor2 = "#3E3E3E";
var fillColor3 = "#3E3E3E";
var fillColor4 = "#3E3E3E";
var fillColor5 = "#3E3E3E";
var fillColor6 = "#3E3E3E";
var fillColor7 = "#3E3E3E";

var playerScore1 = false;
var playerScore2 = false;
var playerScore3 = false;
var playerScore4 = false;
var playerScore5 = false;
var playerScore6 = false;
var playerScore7 = false;
var resetNumber = 0;


let image = new Image();
image.src = "assets/img/spritesheeet.png";
image.onload = gameloop;

const playerMove = true;



//for walk
const walkloop = [0,1,0,2];
const pos = [0,1,2,3];
let contralPos = 0; //position in spritesheeet
let loopIndex = 0;
let frameCount = 0;

const scale = 1; //size of charater
const width = 64;
const height = 82;
const scalewidth = scale * width;
const scaleheight = scale * height;



//win screen
var winScreen = document.getElementById("win-screen");
var winScreen2 = document.getElementById("win-screen2");
var returnBtn = document.querySelector("#win-screen button");




//player setting
function GameObject(spritesheet, x, y, width, height){
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

//Default Player
let player = new GameObject(image, 400, 200, 100, 100);


function clickDpadYellow() {
    gamerInput = new GamerInput("Up");
}

function clickDpadBlue() {
    gamerInput = new GamerInput("Left");
}

function clickDpadRed() {
    gamerInput = new GamerInput("Right");
}

function clickDpadGreen() {
    gamerInput = new GamerInput("Down");
}

function clickableDpadReleased() {
    gamerInput = new GamerInput("None");
}

let yellowButton = document.getElementsByClassName("yellow")[0];
let blueButton = document.getElementsByClassName("blue")[0];
let redButton = document.getElementsByClassName("red")[0];
let greenButton = document.getElementsByClassName("green")[0];


function GamerInput(input){
    console.log("Plyer");
    this.action = input; // Hold the input as a string
}

let gamerInput = new GamerInput("None");

function input(event) { //recognise user input
    if(event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left 
                blueButton.classList.add("pressed");
                gamerInput = new GamerInput("Left");
                break; 
            case 38: // Up 
                yellowButton.classList.add("pressed");
                gamerInput = new GamerInput("Up");
                break; 
            case 39: // Right 
                redButton.classList.add("pressed");
                gamerInput = new GamerInput("Right");
                break;
            case 40: // Down 
                greenButton.classList.add("pressed");
                gamerInput = new GamerInput("Down");
                break; 
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
        redButton.classList.remove("pressed");
        blueButton.classList.remove("pressed");
        yellowButton.classList.remove("pressed");
        greenButton.classList.remove("pressed");
    }
}



function update(){
    console.log("Update");

    if(playerMove == true){

        if(gamerInput.action === "Right"){
            if (player.x >= canvas.width -90){
                player.x = canvas.width -90;
            }
            else{
                player.x += 4;
                contralPos=3;
            }
        }
        else if(gamerInput.action === "Down"){
            if (player.y >= canvas.height -85){
                player.y = canvas.height -85;}
            else{
                player.y += 4;
                contralPos=0;
            }

        }
        else if(gamerInput.action === "Left"){
            if (player.x < 26){
                player.x = 26}
            else{
                player.x -= 4;
                contralPos=2;
            }
        }
        else if(gamerInput.action === "Up"){
            if (player.y < 55){
                player.y = 55;}
            else{
                player.y -= 4;
                contralPos=1;
            }
        } 
    }
    collision();
}

function collision(){ 

    //rambonNumber 1
    if(player.x < x1 + 30 && 
        player.x + 64 > x1 &&
        player.y < y1 + 30 && 
        player.y + 82 > y1
        ){        
            console.log("collision detected"); 
            playernumber = randomNumber1;
            resetNumber = 1;
            
    }

    //rambonNumber 2
    else if(player.x < x2 + 30 && 
        player.x + 64 > x2 &&
        player.y < y2 + 30 && 
        player.y + 82 > y2
        ){        
            console.log("collision detected"); 
            playernumber = randomNumber2;
            resetNumber = 1;
            
    }


    //rambonNumber 3
    else if(player.x < x3 + 30 && 
        player.x + 64 > x3 &&
        player.y < y3 + 30 && 
        player.y + 82 > y3
        ){        
            console.log("collision detected"); 
            playernumber = randomNumber3;
            resetNumber = 1;
                     
    }
    packupNumber();

    if(resetNumber == 1){
        redrawBumber();
    }

}

function packupNumber(){
    console.log(playernumber);
     
    if(playernumber == realPin1){
        fillColor1 = "#26DEFF";
        playerScore1 =true;
    }
    if(playernumber == realPin2){
        fillColor2 = "#26DEFF";
        playerScore2 =true;
    }
    if(playernumber == realPin3){
        fillColor3 = "#26DEFF";
        playerScore3 =true;
    }
    if(playernumber == realPin4){
        fillColor4 = "#26DEFF";
        playerScore4 =true;
    }
    if(playernumber == realPin5){
        fillColor5 = "#26DEFF";
        playerScore5 =true;
    }
    if(playernumber == realPin6){
        fillColor6 = "#26DEFF";
        playerScore6 =true;
    }
    if(playernumber == realPin7){
        fillColor7 = "#26DEFF";
        playerScore7 =true;
    }

    if (playerScore1 == true && playerScore2 == true && playerScore3 == true 
        && playerScore4 == true && playerScore5 == true && playerScore6 == true
        && playerScore7 == true) {

            if(level == "1"){
                winScreen.style.display = "flex";
            }
            else if(level == "2"){
                winScreen.style.display = "flex";
            }
            else if(level == "3"){
                winScreen2.style.display = "flex";
            }
            else{
                winScreen3.style.display = "flex";
            }
            timerRunning = false;

      }
}



function returnToMain() {
    window.location.href = "index.html";// back to Main Page
  }
  




function animation() {
    if (gamerInput.action != "None"){

        frameCount++;

        if (frameCount >= 10) {
            frameCount = 0;
            loopIndex++;

            if (loopIndex >= walkloop.length) {
                loopIndex = 0;
            }
        } 
    }     
    else{
        loopIndex = 0;
    }

    drawFrame(player.spritesheet, walkloop[loopIndex], contralPos, player.x, player.y);
}


function drawFrame(image, frameX, frameY, canvasX, canvasY){
    context.drawImage(image,
         frameX * width, frameY * height, width, height, 
         canvasX, canvasY, scalewidth, scaleheight);
}

function redrawBumber(){

    if(level == "1" || level == "2"){
        randomNumber1 = Math.floor(Math.random() * 10);
        randomNumber2 = Math.floor(Math.random() * 10);
        randomNumber3 = Math.floor(Math.random() * 10);
        }
        
        else if(level == "3"){
            randomNumber1 = getRandomInt(1, 20);
            randomNumber2 = getRandomInt(1, 20);
            randomNumber3 = getRandomInt(1, 20);
        }
        
        
        else{
            randomNumber1 = getRandomInt(1, 30);
            randomNumber2 = getRandomInt(1, 30);
            randomNumber3 = getRandomInt(1, 30);
        }

    x1 = Math.floor(Math.random() * areaWidth) + areaX;
    y1 = Math.floor(Math.random() * areaHeight) + areaY;
    x2 = Math.floor(Math.random() * areaWidth) + areaX;
    y2 = Math.floor(Math.random() * areaHeight) + areaY;
    x3 = Math.floor(Math.random() * areaWidth) + areaX;
    y3 = Math.floor(Math.random() * areaHeight) + areaY;

    resetNumber = 0;

}

function drawNumber(){

    context.fillStyle = "#000000";
    context.font = "40px Arial";
    context.fillText(randomNumber1, x1, y1);
    context.fillText(randomNumber2, x2, y2);
    context.fillText(randomNumber3, x3, y3);

}




function draw(){ //draw image in canvas
    console.log("Draw");
    context.clearRect(0,0,canvas.width, canvas.height);
  

    //Timer
    if(timerRunning == true){
        let now = Date.now();
        let deltaTime = (now - lastUpdateTime) / 1000;
        lastUpdateTime = now;
        countDownTime -= deltaTime;
    }
			
	if (countDownTime <= 0) {
		countDownTime = 0;
        if (confirm("Times Up, you're stuck in the room forever")) {
            countDownTime = 60;
            window.location.href = "index.html";
        }
	}

    context.fillStyle="#000000";
    context.font="20px arial";
    context.fillText("Time left",80,30);
    context.fillText("Door Code",750,25);


    if(countDownTime > 15){
        context.fillStyle="#000000";
    }
    else{
        context.fillStyle="#A91515";
    }

    context.font="30px arial";
    context.fillText(countDownTime.toFixed(1) + " second",130,60);

    //PIN
    context.fillStyle = "#3E3E3E";
    context.fillRect(600, 30, 40, 40);
    context.fillRect(650, 30, 40, 40);
    context.fillRect(700, 30, 40, 40);
    context.fillRect(750, 30, 40, 40);
    context.fillRect(800, 30, 40, 40);
    context.fillRect(850, 30, 40, 40);
    context.fillRect(900, 30, 40, 40);


    context.fillStyle = fillColor1;
    context.font = "35px Arial";
    context.textAlign = "center";
    context.fillText(realPin1, 720, 62);
    context.fillStyle = fillColor2;
    context.fillText(realPin2, 770, 62);
    context.fillStyle = fillColor3;
    context.fillText(realPin3, 820, 62);
    context.fillStyle = fillColor4;
    context.fillText(realPin4, 870, 62);
    context.fillStyle = fillColor5;
    context.fillText(realPin5, 920, 62);
    context.fillStyle = fillColor6;
    context.fillText(realPin6, 670, 62);
    context.fillStyle = fillColor7;
    context.fillText(realPin7, 620, 62);

    drawNumber();

    animation();

}


function gameloop(){
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

window.requestAnimationFrame(gameloop);

window.addEventListener('keydown', input);
window.addEventListener('keyup', input);